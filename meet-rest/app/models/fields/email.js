const _validate = (v) => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,50})+$/.test(v);

const Field = {
  type: String,
  validate: [_validate, 'Email inválido'],
  unique: true,
  required: [
    () => { return (this.email != null || this.email != '' || this.email != undefined); },
    'Campo \'Email\' é obrigatório'
]
}

module.exports = Field
