var mongoose = require('mongoose');

var schema = mongoose.Schema({
  usuario: {
    type: String,
    ref: 'Usuario',
    required: [
      () => {
        return (this.usuario != null || this.usuario != '' || this.usuario != undefined);
      },
      '\'Usuario\' é obrigatório'
    ]
  },
  categoria: {
    type: String,
    ref: 'Categoria',
    required: [
      () => {
        return (this.categoria != null || this.categoria != '' || this.categoria != undefined || this.categoria != []);
      },
      '\'Categoria\' é obrigatória'
    ]
  }
});

mongoose.model('Preferencia', schema, 'preferencias');
