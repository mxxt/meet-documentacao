var mongoose = require('mongoose');
var email = require('./fields/email');

var schema = mongoose.Schema({
    nome: {
        type: String,
        required: [
            () => { return (this.nome != null || this.nome != '' || this.nome != undefined);},
            '\'Nome\' é obrigatório'
        ]
    },
    email: email,
    nascimento : {
      type: Date,
      required: [
          () => { return (this.nascimento != null || this.nascimento != '' || this.nascimento != undefined);},
          '\'Nascimento\' é obrigatório'
      ]
    },
    cidade: {
      type: String,
      ref: 'Cidade'
    },
    usuario: {
      type: String,
      unique: true,
      required: [
          () => { return (this.usuario != null || this.usuario != '' || this.usuario != undefined);},
          '\'Usuario\' é obrigatório'
      ]
    },
    sexo: {
      type: String
    },
    criadoEm: {
      type: Date
    },
    atualizadoEm: {
      type: Date
    },
    senha: {
        type: String,
        required: [
            () => { return (this.senha != null || this.senha != '' || this.senha != undefined); },
            '\'Senha\' é obrigatório'
        ]
    }
});

mongoose.model('Usuario', schema, 'usuarios');
