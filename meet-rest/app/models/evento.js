var mongoose = require('mongoose');

var schema = mongoose.Schema({
  categoria: {
    type: String,
    ref: 'Categoria',
    required: [
      () => {
        return (this.categoria != null || this.categoria != '' || this.categoria != undefined);
      },
      '\'Categoria\' é obrigatória'
    ]
  },
  descricao: {
    type: String,
    required: [
      () => {
        return (this.descricao != null || this.descricao != '' || this.descricao != undefined);
      },
      '\'Descricao\' é obrigatória'
    ]
  },
  criadoPor: {
    type: String,
    ref: 'Usuario',
    required: [
      () => {
        return (this.criadoPor != null || this.criadoPor != '' || this.criadoPor != undefined);
      },
      '\'Usuario\' é obrigatório'
    ]
  },
  localizacao: {
    type: {},
    required: [
      () => {
        return (this.localizazao != null || this.localizazao != '' || this.localizazao != undefined);
      },
      '\'Local\' é obrigatório'
    ]
  },
  ticket: {
    type: String
  },
  inicio: {
    type: Date,
    required: [
      () => {
        return (this.inicio != null || this.inicio != '' || this.inicio != undefined);
      },
      '\'Inicio\' é obrigatório'
    ]
  },
  fim: {
    type: Date,
    required: [
      () => {
        return (this.fim != null || this.fim != '' || this.fim != undefined);
      },
      '\'Fim\' é obrigatório'
    ]
  },
  criadoEm: {
    type: Date
  },
  atualizadoEm: {
    type: Date
  }
});

mongoose.model('Evento', schema, 'eventos');
