var mongoose = require('mongoose');

var schema = mongoose.Schema({
  usuario: {
    type: String,
    ref: 'Usuario',
    required: [
      () => {
        return (this.suario != null || this.suario != '' || this.suario != undefined);
      },
      'Usuario é obrigatório'
    ]
  },
  path: {
    type: String,
    required: [
      () => {
        return (this.path != null || this.path != '' || this.path != undefined);
      },
      'Path da imagem é obrigatório'
    ]
  },
  tipo: {
    type: String,
    maxlength: 2,
    required: [
      () => {
        return (this.tipo != null || this.tipo != '' || this.tipo != undefined);
      },
      'Tipo de upload é obrigatório'
    ]
  }
});

mongoose.model('Upload', schema, 'uploads');
