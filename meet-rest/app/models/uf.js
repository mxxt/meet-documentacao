var mongoose = require('mongoose');

var schema = mongoose.Schema({
  uf: {
    type: String,
    required: [
      () => {
        return (this.uf != null || this.uf != '' || this.uf != undefined);
      },
      '\'UF\' é obrigatória'
    ]
  },
  regiao: {
    type: String,
    required: [
      () => {
        return (this.regiao != null || this.regiao != '' || this.regiao != undefined);
      },
      '\'Região\' é obrigatória'
    ]
  },
  nome: {
    type: String,
    required: [
      () => {
        return (this.nome != null || this.nome != '' || this.nome != undefined);
      },
      '\'Nome\' é obrigatório'
    ]
  }
});

mongoose.model('UF', schema, 'ufs');
