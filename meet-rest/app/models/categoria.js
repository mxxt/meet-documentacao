var mongoose = require('mongoose');

var schema = mongoose.Schema({
  nome: {
    type: String,
    unique: true,
    required: [
      () => {
        return (this.nome != null || this.nome != '' || this.nome != undefined);
      },
      '\'Categoria\' é obrigatória'
    ]
  }
});

mongoose.model('Categoria', schema, 'categorias');
