var mongoose = require('mongoose');

var schema = mongoose.Schema({
  usuario: {
    type: String,
    ref: 'Usuario',
    required: [
      () => {
        return (this.usuario != null || this.usuario != '' || this.usuario != undefined);
      },
      '\'Usuario\' é obrigatório'
    ]
  },
  evento: {
    type: String,
    ref: 'Evento',
    required: [
      () => {
        return (this.evento != null || this.evento != '' || this.evento != undefined || this.evento != []);
      },
      '\'Evento\' é obrigatório'
    ]
  }
});

mongoose.model('LikeEvento', schema, 'likeeventos');
