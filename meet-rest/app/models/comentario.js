var mongoose = require('mongoose');

var schema = mongoose.Schema({
  usuario: {
    type: String,
    ref: 'Usuario',
    required: [
      () => { return (this.usuario != null || this.usuario != '' || this.usuario != undefined); },
        '\'Usuario\' é obrigatório'
    ]
  },
  post: {
    type: String,
    ref: 'Post',
  },
  evento: {
    type: String,
    ref: 'Evento',
  },
  comentario: {
    type: String,
    ref: 'Comentario',
  }
});

mongoose.model('Comentario', schema, 'comentarios');
