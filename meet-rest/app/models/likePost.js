var mongoose = require('mongoose');

var schema = mongoose.Schema({
  usuario: {
    type: String,
    ref: 'Usuario',
    required: [
      () => {
        return (this.usuario != null || this.usuario != '' || this.usuario != undefined);
      },
      '\'Usuario\' é obrigatório'
    ]
  },
  objetoCurtido: {
    type: String,
    ref: 'Post',
    required: [
      () => {
        return (this.objetoCurtido != null || this.objetoCurtido != '' || this.objetoCurtido != undefined || this.objetoCurtido != []);
      },
      '\'Categoria\' é obrigatória'
    ]
  }
});

mongoose.model('LikePost', schema, 'likeposts');
