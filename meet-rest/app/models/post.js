var mongoose = require('mongoose');

var schema = mongoose.Schema({
  evento: {
    type: String,
    ref: 'Evento',
    required: [
      () => {
        return (this.evento != null || this.evento != '' || this.evento != undefined);
      },
      '\'Post\' é obrigatório'
    ]
  },
  texto: {
    type: String,
    required: [
      () => {
        return (this.texto != null || this.texto != '' || this.texto != undefined);
      },
      '\'Texto\' é obrigatório'
    ]
  }
});

mongoose.model('Post', schema, 'posts');
