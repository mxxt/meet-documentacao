var mongoose = require('mongoose');

var schema = mongoose.Schema({
  seguido: {
    type: String,
    ref: 'Usuario',
    required: [
      () => { return (this.seguido != null || this.seguido != '' || this.seguido != undefined); },
      '\'Seguido\' é obrigatório'
    ]
  },
  seguidor: {
      type: String,
      ref: 'Usuario',
      required: [
        () => { return (this.seguidor != null || this.seguidor != '' || this.seguidor != undefined); },
        '\'Seguidores\' são obrigatório'
      ]
  }
});

mongoose.model('Amigo', schema, 'amigos');
