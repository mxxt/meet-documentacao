module.exports = (app) => {

    var api = app.api.uf;

    app.get('/uf/auth/listar', api.listar);
    app.post('/uf/augh/popularUFs', api.popularUFs);
    app.post('/uf/auth/limparUFs', api.limparUFs);
    app.route('/uf/auth/:id')
      .get(api.buscarPorId)
};
