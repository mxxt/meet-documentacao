module.exports = (app) => {

    var api = app.api.cidade;

    app.get('/cidade/auth/listar', api.listar);
    app.post('/cidade/augh/popularCidades', api.popularCidades);
    app.post('/cidade/auth/limparCidades', api.limparCidades);
    app.route('/cidade/auth/:id')
      .get(api.buscarPorId)
};
