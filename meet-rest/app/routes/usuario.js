module.exports = (app) => {
  var api = app.api.usuario;

  app.post('/usuario/criar', api.criarUsuario);
  app.post('/usuario/autenticar', api.autenticar);
  app.post('/usuario/auth/sair', api.sair);
  app.get('/usuario/auth/listar', api.listar);
  app.get('/usuario/auth/:id/totalSeguidores', api.totalSeguidores);
  app.get('/usuario/auth/:id/totalSeguindo', api.totalSeguindo);
  app.post('/usuario/auth/codigoConfirmacao', api.enviarCodigoConfirmacao);
  app.route('/usuario/auth/:id')
    .get(api.buscarPorId)
    .delete(api.removerPorId)
    .put(api.atualizarPorId);
};
