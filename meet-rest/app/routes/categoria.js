module.exports = (app) => {

    var api = app.api.categoria;

    app.post('/categoria/criar', api.criarCategoria);
    app.post('/categoria/popularCategorias', api.popularCategorias);
    app.post('/categoria/limparCategorias', api.limparCategorias);
    app.get('/categoria/listar', api.listar);
    app.route('/categoria/id/:id')
      .get(api.buscarPorId)
      .delete(api.removerPorId)
      .put(api.atualizarPorId);
};
