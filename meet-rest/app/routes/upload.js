module.exports = (app) => {
  var api = app.api.upload;

  app.post('/upload/auth/upload', api.upload);
  app.post('/upload/auth/listarPorTipoUsuario', api.listarPorTipoUsuario);
  app.route('/upload/auth/:id')
    .delete(api.removerPorId)
    .put(api.atualizarPorId);
};
