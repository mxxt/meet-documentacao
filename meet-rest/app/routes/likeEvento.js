module.exports = (app) => {
  var api = app.api.likeEvento;

  app.post('/likeEvento/auth/curtir', api.curtir);
  app.get('/likeEvento/auth/listar', api.listar);
  app.get('/likeEvento/auth/total', api.total);
  app.get('/likeEvento/auth/:idUsuario/total', api.totalPorUsuario);
  app.route('/likeEvento/auth/:id')
    .get(api.buscarPorId)
    .delete(api.descurtir);
};
