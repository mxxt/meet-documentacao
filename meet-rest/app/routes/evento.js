module.exports = (app) => {

    var api = app.api.evento;

    app.post('/evento/auth/criar', api.criar);
    app.get('/evento/auth/listar', api.listar);
    app.route('/evento/auth/:id')
      .get(api.buscarPorId)
      .delete(api.removerPorId)
      .put(api.atualizarPorId);
};
