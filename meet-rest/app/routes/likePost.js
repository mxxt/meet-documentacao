module.exports = (app) => {
  var api = app.api.likePost;

  app.post('/likePost/auth/curtir', api.curtir);
  app.get('/likePost/auth/listar', api.listar);
  app.route('/likePost/auth/:id')
    .get(api.buscarPorId)
    .delete(api.descurtir);
};
