module.exports = (app) => {

    var api = app.api.amigo;

    app.post('/amigo/auth/seguir', api.seguir);
    app.get('/amigo/auth/seguidores', api.listarSeguidores);
    app.delete('/amigo/auth/pararDeSeguir', api.pararDeSeguir);
};
