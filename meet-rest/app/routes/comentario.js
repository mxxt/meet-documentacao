module.exports = (app) => {
  var api = app.api.comentario;

  app.post('/comentario/auth/comentar', api.comentar);
  app.get('/comentario/auth/listar', api.listar);
  app.get('/comentario/auth/:idUsuario/:idEvento', api.listarPorUsuarioEvento);
  app.get('/comentario/auth/:idUsuario/:idPost', api.listarPorUsuarioPost);
  app.get('/comentario/auth/:idUsuario/:idComentario', api.listarPorUsuarioComentario);
  app.route('/comentario/auth/:id')
    .get(api.buscarPorId)
    .delete(api.editar);
};
