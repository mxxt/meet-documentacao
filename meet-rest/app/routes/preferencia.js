module.exports = (app) => {

    var api = app.api.preferencia;

    app.post('/preferencia/auth/criar', api.criarPreferencia);
    app.get('/preferencia/auth/listar', api.listar);
    app.route('/preferencia/auth/:id')
      .get(api.buscarPorId)
      .delete(api.removerPorId)
      .put(api.atualizarPorId);
};
