module.exports = (app) => {

    var api = app.api.post;

    app.post('/post/auth/postar', api.postar);
    app.get('/post/auth/listar', api.listar);
    app.route('/post/auth/:id')
      .get(api.buscarPorId)
      .delete(api.removerPorId)
      .put(api.atualizarPorId);
};
