var mongoose = require('mongoose');
var populaDB = require('../../config/scripts/populaCidades');
var api = {};

var model = mongoose.model('Cidade');

api.popularCidades = (req, res) => {
  var cidadesDB = populaDB(model);

  model.insertMany(cidadesDB, {}).then((cidades) => {
    res.status(200).json(cidades);
    console.log('Cidades populadas com sucesso');
  }, (error) => {
    res.status(500).json(error);
  });
}

api.limparCidades = (req, res) => {
  model.remove({})
    .then(() => {
    res.sendStatus(200);
  }, (error) => {
    res.status(500).json(error);
  });
}

api.listar = (req, res) => {
  model.find()
    .populate('uf', 'nome regiao uf')
    .then((ufcidades) => {
    res.status(200).json(ufcidades);
  }, (error) => {
    console.log(error);
    res.status(500).json(error);
  });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .populate('uf', 'nome regiao uf')
    .then((ufcidade) => {
    if (!ufcidade)
      res.status(204).json(ufcidade);
    res.status(200).json(ufcidade);
  }, (error) => {
    console.log(error);
    res.status(500).json(error);
  });
};

module.exports = api;
