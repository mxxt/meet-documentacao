var mongoose = require('mongoose');
var api = {};
var model = mongoose.model('Amigo');

api.seguir = (req, res) => {
  var amigos = req.body;

  model.insertMany(amigos, {})
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listarSeguidores = (req, res) => {
  model.find({seguido: req.params.id})
    .then((amigos) => {
      if (!amigos) res.sendStatus(204);
      res.status(200).json(amigos);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.pararDeSeguir = (req, res) => {
  model.remove(req.body)
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
