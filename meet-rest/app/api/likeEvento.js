var mongoose = require('mongoose');

var api = {};
var model = mongoose.model('LikeEvento');

api.curtir = (req, res) => {
  var like = req.body;

model.create(like)
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .populate('usuario', 'nome email')
    .then((likes) => {
      res.status(200).json(likes);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

//Total de curtidas em cada evento por usuário
api.totalPorUsuario = (req, res) => {
  var usuarioId = req.params.id;
  model.count({usuario: usuarioId})
    .then((total) => {
      res.status(200).json(total);
    }, (error) => {
      res.status(500).json(error);
    });
}

api.total = (req, res) => {
  var usuarioId = req.params.id;
  console.log(usuarioId);
  model.count({usuario: usuarioId})
    .then((total) => {
      res.status(200).json(total);
    }, (error) => {
      res.status(500).json(error);
    });
}

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .then((like) => {
      if (!like) res.status(204).json(like);
      res.status(200).json(like);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.descurtir = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
