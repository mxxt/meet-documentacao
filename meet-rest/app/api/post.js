var mongoose = require('mongoose');
var api = {};
var model = mongoose.model('Post');

api.postar = (req, res) => {
  var post = req.body;

  model.create(posts)
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .then((posts) => {
      res.status(200).json(posts);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .then((post) => {
      if (!post) res.status(404).json({mensagem: 'Post não encontrado'});
      res.status(200).json(post);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.comentarios = (req, res) => {
  var idPost = req.params.idPost;
  modelomentario.find({post: idPost}, {})
    .then((comentarios) => {
      if (!comentarios) res.status(204).json(comentarios);
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
}

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.atualizarPorId = (req, res) => {
  model.findByIdAndUpdate(req.params.id, req.body, {})
    .then((post) => {
      if (!post) res.status(401).json(post);
      res.status(200).json(post);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
