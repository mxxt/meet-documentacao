var mongoose = require('mongoose');

var api = {};

var model = mongoose.model('Comentario');

api.comentar= (req, res) => {
  var comentarios = req.body;

  model.insertMany(comentarios, {})
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .then((comentarios) => {
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .then((comentario) => {
      if (!comentario) res.status(204).json(comentario);
      res.status(200).json(comentario);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listarPorUsuarioEvento = (req, res) => {
  var idUsuario = req.params.idUsuario;
  var idEvento = req.params.idEvento;
  model.find({usuario: idUsuario, evento: idEvento}, {})
    .then((comentarios) => {
      if (!comentarios) res.status(204).json(comentarios);
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listarPorUsuarioPost = (req, res) => {
  var idUsuario = req.params.idUsuario;
  var idPost = req.params.idPost;
  model.find({usuario: idUsuario, evento: idPost}, {})
    .then((comentarios) => {
      if (!comentarios) res.status(204).json(comentarios);
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listarPorUsuarioComentario = (req, res) => {
  var idUsuario = req.params.idUsuario;
  var idComentario = req.params.idComentario;
  model.find({usuario: idUsuario, evento: idComentario}, {})
    .then((comentarios) => {
      if (!comentarios) res.status(204).json(comentarios);
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.editar = (req, res) => {
  model.findByIdAndUpdate(req.params.id, req.body, {})
    .then((comentario) => {
      if (!comentario) res.status(401).json(comentario);
      res.status(200).json(comentario);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
