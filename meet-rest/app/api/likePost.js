var mongoose = require('mongoose');

var api = {};
var model = mongoose.model('LikePost');

api.curtir = (req, res) => {
  var like = req.body;

  model.create(likes)
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .then((likes) => {
      res.status(200).json(likes);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .then((like) => {
      if (!like) res.status(404).json(like);
      res.status(200).json(like);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.descurtir = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
