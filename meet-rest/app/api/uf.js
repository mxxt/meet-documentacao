var mongoose = require('mongoose');
var populaDB = require('../../config/scripts/populaUFs');
var api = {};

var model = mongoose.model('UF');

api.popularUFs = (req, res) => {
  var UFsDB = populaDB(model);

  model.insertMany(UFsDB, {})
    .then((ufs) => {
    res.status(200).json(ufs);
    console.log('UFs populadas com sucesso');
  }, (error) => {
    res.status(500).json(error);
  });
}

api.limparUFs = (req, res) => {
  model.remove({}).then(() => {
    res.sendStatus(200);
  }, (error) => {
    res.status(500).json(error);
  });
}

api.listar = (req, res) => {
  model.find()
    .then((ufs) => {
    res.status(200).json(ufs);
  }, (error) => {
    console.log(error);
    res.status(500).json(error);
  });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {}).then((ufcidade) => {
    if (!ufcidade)
      res.status(204).json(ufcidade);
    res.status(200).json(ufcidade);
  }, (error) => {
    console.log(error);
    res.status(500).json(error);
  });
};

module.exports = api;
