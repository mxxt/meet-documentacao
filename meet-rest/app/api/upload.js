var mongoose = require('mongoose');
var fs = require('fs');
var api = {};
var model = mongoose.model('Upload');

api.upload = (req, res) => {
  var upload = req.body;
  var fileName = req.headers.filename;
  var path = '';

  if (fileName) {
    path = 'config/files/' + fileName;
    upload = {
      ...upload,
      path: avatarPath
    };
  } else {
    res.status(500).json({mensagem: 'Nome do arquivo inválido'});
  }

  req.pipe(fs.createWriteStream(path)).on('finish', function() {
    model.create(upload).then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
  });
};

api.listarPorTipoUsuario = (req, res) => {
  model.find({usuario: req.body.idUsuario, tipo: req.body.tipo}).populate('usuario', 'nome email').then((eventos) => {
    res.status(200).json(eventos);
  }, (error) => {
    console.log(error);
    res.status(500).json(error);
  });
};

api.removerPorId = (req, res) => {
};

api.atualizarPorId = (req, res) => {
};

module.exports = api;
