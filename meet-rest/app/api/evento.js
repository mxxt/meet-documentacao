var mongoose = require('mongoose');

var api = {};

var model = mongoose.model('Evento');
var model = mongoose.model('Comentario');

api.criar = (req, res) => {
  var evento = req.body;

  model.create(evento)
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .then((eventos) => {
      res.status(200).json(eventos);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.comentarios = (req, res) => {
  var idEvento = req.params.idEvento;
  modelomentario.find({evento: idEvento}, {})
    .then((comentarios) => {
      if (!comentarios) res.status(204).json(comentarios);
      res.status(200).json(comentarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
}

api.buscarPorId = (req, res) => {
  model.findById(req.params.id)
    .then((evento) => {
      if (!evento) res.status(204).json(evento);
      res.status(200).json(evento);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.atualizarPorId = (req, res) => {
  model.findByIdAndUpdate(req.params.id, req.body, {})
    .then((evento) => {
      if (!evento) res.status(401).json(evento);
      res.status(200).json(evento);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
