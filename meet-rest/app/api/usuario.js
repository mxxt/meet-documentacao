var mongoose = require('mongoose');
var mailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var sha512 = require('sha512');
var securePin = require('secure-pin');
var fs = require('fs');

var api = {};
var model = mongoose.model('Usuario');
var modelAmigo = mongoose.model('Amigo');

api.criarUsuario = (req, res) => {
  var usuario = req.body;

  if (usuario.senha) usuario.senha = sha512(usuario.senha);

  model.create(usuario)
    .then((resultado) => {
      resultado.senha = '***';
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model
    .find({}, {senha: 0})
    .populate('cidadde', 'nome')
    .then((usuarios) => {
      res.status(200).json(usuarios);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {
      senha: 0
    })
    .then((usuario) => {
      if (!usuario) res.sendStatus(404).json({mensagem: 'Usuário não encontrado.'});
      res.status(200).json(usuario);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.atualizarPorId = (req, res) => {
  var senha = req.body.senha;
  if (senha) {
    senha = sha512(senha);
    req.body.senha = senha;
  }
  model.findByIdAndUpdate(req.params.id, req.body, {})
    .then((usuario) => {
      if (!usuario) res.status(401).json(usuario);
      res.status(200).json(usuario);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.totalSeguidores = (req, res) => {
  var usuarioId = req.params.id;
  modelAmigo.count({usuario: usuarioId})
    .then((total) => {
      res.status(200).json(total);
    }, (error) => {
      res.status(500).json(error)
    })
}

api.totalSeguindo = (req, res) => {
  var usuarioId = req.params.id;
  modelAmigo.count({usuario: usuarioId})
    .then((total) => {
      res.status(200).json(total);
    }, (error) => {
      res.status(500).json(error)
    })
}

api.autenticar = (req, res) => {
  var login = req.body.usuario;
  var senha = req.body.senha;
  if (senha) {
    senha = sha512(senha);
    req.body.senha = senha;
  }
  model.findOne({usuario: login, senha: senha}, {
      senha: 0
    })
    .then((usuario) => {
      if (!usuario) {
        res.status(401).json({mensagem: 'Algo está errado, verifique as informações e tente novamente.'});
      } else {
        var token = jwt.sign({login: usuario.usuario}, req.app.get('meetId'), {expiresIn: 1000000});
        res.set('x-meet-access-token', token);
        res.set('login', usuario.usuario);
        res.status(200).json({mensagem: 'OK', token: token, usuario: usuario});
      }
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.sair = (req, res) => {
  var login = req.body.usuario;
  model.findOne({usuario: login}, {
      senha: 0
    })
    .then((usuario) => {
      if (!usuario) {
        res.status(401).json({mensagem: 'Algo está errado, verifique as informações e tente novamente.'});
      } else {
        var token = jwt.sign({login: usuario.usuario}, req.app.get('meetId'), {expiresIn: 0});
        res.set('x-meet-access-token', token);
        res.status(200).json({mensagem: 'sessão encerrada', token: token});
      }
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.enviarCodigoConfirmacao = (req, res) => {
  var email = req.body;

  //Geração do código de confirmação
  var pin = securePin.generatePinSync(6);
  console.log(pin);

  //Configurar email do meet
  let transporter = mailer.createTransport({
    service: 'hotmail',
    auth: {
      user: 'emaildomeet',
      pass: 'senha do email'
    }
  });

  let mailOptions = {
    //from é email do meet e to é o email do usuario
    from: 'goj1@hotmail.com',
    to: req.body.emal,
    subject: 'Código de confirmação Meet',
    text: 'Olá ' + req.body.usuario + ', seu código de verificação é: ' + pin
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      res.status(500).json(error);
    } else {
      res.status(200).json({
        Info: 'Email enviado com sucesso!'
      });
    }
  });
};

module.exports = api;
