var mongoose = require('mongoose');
var categoriasDB = require('../../config/scripts/populaCategoriaDB');
var api = {};

var model = mongoose.model('Categoria');

api.popularCategorias = (req, res) => {
  categoriasDB = categoriasDB(model);
  model.insertMany(categoriasDB, {})
    .then((categorias) => {
      res.status(200).json(categorias);
      console.log('Categorias populadas com sucesso');
    }, (error) => {
      res.status(500).json(error);
    });
}

api.limparCategorias = (req, res) => {
    model.remove({})
      .then(() => {
        res.sendStatus(200);
      }, (error) => {
        res.status(500).json(error);
      });
}

api.criarCategoria = (req, res) => {
  var categorias = req.body;

  model.insertMany(categorias, {})
  .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model
    .find()
    .then((categorias) => {
      res.status(200).json(categorias);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .then((categoria) => {
      if (!categoria) res.status(204).json(categoria);
      res.status(200).json(categoria);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.atualizarPorId = (req, res) => {
  model.findByIdAndUpdate(req.params.id, req.body, {new: true})
    .then((categoria) => {
      if (!categoria) res.status(401).json(categoria);
      res.status(200).json(categoria);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
