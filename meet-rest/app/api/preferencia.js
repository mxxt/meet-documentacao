var mongoose = require('mongoose');

var api = {};
var model = mongoose.model('Preferencia');

api.criarPreferencia = (req, res) => {
  var preferencia = req.body;

  model.create(preferencias)
    .then((resultado) => {
      res.status(200).json(resultado);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.listar = (req, res) => {
  model.find()
    .populate('usuario', 'nome usuario email')
    .populate('categoria', 'nome _id')
    .then((preferencias) => {
      res.status(200).json(preferencias);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.buscarPorId = (req, res) => {
  model.findById(req.params.id, {})
    .populate('usuario', 'nome usuario email')
    .populate('categoria', 'nome _id')
    .then((preferencia) => {
      if (!preferencia) res.status(204).json(preferencia);
      res.status(200).json(preferencia);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.removerPorId = (req, res) => {
  model.remove({
      _id: req.params.id
    })
    .then(() => {
      res.sendStatus(200);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

api.atualizarPorId = (req, res) => {
  model.findByIdAndUpdate(req.params.id, req.body, {new: true})
    .then((preferencia) => {
      if (!preferencia) res.status(401).json(preferencia);
      res.status(200).json(preferencia);
    }, (error) => {
      console.log(error);
      res.status(500).json(error);
    });
};

module.exports = api;
