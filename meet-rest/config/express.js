var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var middlewareToken = require('./auth');
var middlewareCors = require('./cors');

var app = express();
// middlewareCors(app);
middlewareToken(app, express);


// parse application/x-www-form-urlencoded
// Id com criptografia de 256 bits.
app.set('meetId', 'WXMpsJwp3i6fWX3HpwnfCw5JW7ohRvD0');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressValidator());

//Limite de tamanho para fotos.
app.use(bodyParser.json({limit: '30mb'}));

consign({cwd: 'app'}).include('models').then('api').then('routes').into(app);

module.exports = app;
