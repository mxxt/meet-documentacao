module.exports = (app, express) => {
  var jwt = require('jsonwebtoken');
  var router = express.Router();

  router.use('/amigo/auth/seguir', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/amigo/auth/seguidores', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/amigo/auth/pararDeSeguir', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/categora/auth/criar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/categoria/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/categoria/auth/popularCategorias', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/categoria/auth/limparCategorias', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/categoria/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/cidade/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/cidade/augh/popularCidades', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/cidade/auth/limparCidades', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/cidade/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/comentario/auth/comentar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/comentario/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/comentario/auth/:idUsuario/:idEvento', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/comentario/auth/:idUsuario/:idPost', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/comentario/auth/:idUsuario/:idComentario', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/comentario/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/evento/auth/criar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/evento/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/evento/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/likeEvento/auth/curtir', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likeEvento/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likeEvento/auth/total', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likeEvento/auth/:idUsuario/total', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likeEvento/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/likePost/auth/curtir', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likePost/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/likePost/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/post/auth/postar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/post/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/post/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/preferencia/auth/criar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/preferencia/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/preferencia/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/uf/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/uf/augh/popularUFs', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/uf/auth/limparUFs', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/uf/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/upload/auth/upload', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/upload/auth/listarPorTipoUsuario', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/upload/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  router.use('/usuario/auth/listar', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/usuario/auth/sair', (req, res, next) => {
    check(req, res, next);
  });
  router.use('/usuario/auth/:id', (req, res, next) => {
    check(req, res, next);
  });

  function check(req, res, next) {
    var token = req.headers['x-meet-access-token'];
    var reqUsuario = req.headers['login'];
    if (token) {
      jwt.verify(token, app.get('meetId'), (err, decoded) => {
        if (err /*|| reqUsuario !== decoded.login*/) {
          res.status(401).json({messagem: 'Autentique-se para realizar essa ação.'});
        } else {
          //implementar aqui a lógica para verificação de usuario após verificar a existênia do primeiro token caso necessário
          req.usuario = decoded;
          next();
        }
      });
    } else {
      return res.status(401).json({
        mensagem: 'Acesso não autorizado'
      });
    }
  }

  app.use('/', router);
}
