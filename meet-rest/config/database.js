module.exports = (uri) => {
  var mongoose = require('mongoose');

  mongoose.Promise = global.Promise;

  mongoose.connect('mongodb://' + uri);

  mongoose.connection.on('connected', () => {
    console.log('Conectado ao banco de dados!');
    console.log();
  });

  mongoose.connection.on('erro', (error) => {
    console.log('Erro na conexao: ' + error);
  });

  mongoose.connection.on('disconnected', () => {
    console.log('Desconectado do servidor');
  });

  process.on('SIGINT', () => {
    mongoose.connection.close(() => {
      console.log('Conexão com MongoDB fechada pelo término do serviço.');
      process.exit(0);
    });
  });
}
