module.exports = (app) => {

  app.use(function(req, res, next) {
    console.log('CORS habilitado');
    res.header('Access-Control-Allow-Origin', 'http://informar aqui o domínio do meet');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
}
