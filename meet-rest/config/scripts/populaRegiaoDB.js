module.exports = (regiao) => {
  return [
    new regiao({nome: 'Norte', codigo: 1}),
    new regiao({nome: 'Nordeste', codigo: 2}),
    new regiao({nome: 'Centro-Oeste', codigo: 3}),
    new regiao({nome: 'Sul', codigo: 4}),
    new regiao({nome: 'Sudeste', codigo: 5})
  ];
}
