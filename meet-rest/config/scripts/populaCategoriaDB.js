module.exports = (categoria) => {
  return [
    new categoria({nome: 'Festas e Shows'}),
    new categoria({nome: 'Congresso e Seminário'}),
    new categoria({nome: 'Curso e Workshop'}),
    new categoria({nome: 'Teatro'}),
    new categoria({nome: 'Esportivo'}),
    new categoria({nome: 'Gastronômico'})
  ];
}
