const _validate = (v) => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,50})+$/.test(v);

const Field = {
  type: String,
  validate: [_validate, 'Email({VALUE}) inválido'],
  unique: true
}

module.exports = Field
