var http = require('http');

require('./config/database')('localhost/meet');
var app = require('./config/express');

http.createServer(app).listen(3000, () => {
  console.log();
  console.log('Servidor Meet online!');
});
