Serviços backend ouvidoria

Formato:

* rota
* métodos
* especificação
* parametros (quando necessário)


URL base: http://localhost:3000


Anexos
```
/:id_demanda/:id_anexo
GET e DELETE
retorna ou deleta um arquivo
```
```
/inserirAnexoExterno/:id
POST
adiciona um arquivo a demanda de id = :id
Obs: arquivos anexados pelos demandantes usam este serviço. Tamanho máximo de 20mb
```
```
/inserirAnexoInterno/:id
POST
adiciona um arquivo a demanda de id = :id 
Obs: arquivos anexados pelos servidores da ouvidoria usam este serviço. Tamanho máximo de 20mb
```

Auth
```
/autenticar
POST
responsável pela autenticação do usuário
recebe no body da requisição os dados do usuário, no caso email e senha
```

Classificação da demanda
```
/classificacao/cria
POST
responsável pela criação de uma nova classificação de demandas
recebe no body da requisição uma string específicando o nome da classificação
```
```
/classificacao/lista
GET
retorna todas as classificações existentes no banco
```
```
/classificacao/:id
PUT E DELETE
deleta ou atualiza uma classificação com id = :id
```

Demanda
```
/demanda/cria
POST
responsável pela criação de uma demanda
recebe no body da requisição os dados da demanda e do manifestante responsável
```
```
/demanda/lista
GET
retorna todas as demandas do sistema
```
```
/demanda/:id
GET e PUT
responsável por atualizar ou buscar uma demanda com id = :id
```

UF e Municipios
```
/estado/lista
GET
responsável por listar todos os estados
```
```
/cidade/lista/:sigla
GET
lista todas as cidades pertencentes a ao estado com id = :id
```

Tramitacao da demanda
```
/tramita/analise/:id
POST
tramita a demanda com id = :id para o estado de Em Análise
```
```
/tramita/retorna/:id
POST
retorna a demanda com id = :id para uma tramitação anterior
```
```
/tramita/arquivada/:id
POST
arquiva a demanda
```
```
/tramita/definitivo/:id
POST
passa para o estado Em Recebimento Definitivo
```
```
/tramita/concluida/:id
POST
passa para o estado de Concluída
```
```
/tramita/revisao/:id
POST
passa para o estado de revisão
```
```
/tramita/esclarecimento/:id_demanda/:cod_unidade_destino
POST
envia demanda com id = :id_demanda para esclarecimento da unidade com código = :cod_unidade_destino
```
```
/tramita/puxar/:id
POST
puxa demanda enviada para unidade, retornando para o estado de recebimento definitivo
```
```
/tramita/ciencia/:id_demanda/:cod_unidade_destino
POST
envia demanda com id = :id_demanda para ciencia da unidade com código = :cod_unidade_destino
```
```
/tramita/ciente/:id
POST
tramita para ciente
```
```
/tramita/esclarecida/:id
POST
tramita para esclarecida
```
```
/tramita/devolvida/:id
POST
tramita a demanda para devolvida para ouvidoria
```

Unidades
```
/unidade/cria
POST
cria uma nova unidade no sistema
nome, sigla e email é necessário no body da requisição
```
```
/unidade/lista
GET
retorna unidades do sistema
```
```
/unidade/:id
GET
retorna unidade
DELETE
deleta unidade
PUT
atualiza dados unidade
```

Usuario
```
/usuario/cria
POST
cria um novo usuário do sistema
```
```
/usuario/lista
GET
lista os usuário do sistema
```
```
/usuario/:id
GET
retorna usuário cujo id = :id
DELETE
deleta usuario
PUT
atualiza dados do usuario
```