angular.module('starter.controllers', [])
    .controller('MenuCtrl', function($scope) {})
    .controller('LoginCadastroCtrl', function($scope, $ionicModal) {
      angular.element(document.getElementById("content")).removeClass("has-header");
      $ionicModal.fromTemplateUrl('templates/esqueceu_senha.html', {
        id: '1', // We need to use and ID to identify the modal that is firing the event!
        scope: $scope,
        backdropClickToClose: false,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.oModal1 = modal;
      });
      // Modal 2
      $ionicModal.fromTemplateUrl('templates/codigo_verificacao.html', {
      id: '2', // We need to use and ID to identify the modal that is firing the event!
      scope: $scope,
      backdropClickToClose: false,
      animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.oModal2 = modal;
      });
      $ionicModal.fromTemplateUrl('templates/nova_senha.html', {
        id: '3', // We need to use and ID to identify the modal that is firing the event!
        scope: $scope,
        backdropClickToClose: false,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.oModal3 = modal;
      });
      $ionicModal.fromTemplateUrl('templates/senha_criada.html', {
        id: '4', // We need to use and ID to identify the modal that is firing the event!
        scope: $scope,
        backdropClickToClose: false,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.oModal4 = modal;
      });
      $scope.openModal = function(index) {
        if (index == 1) {
          $scope.oModal1.show();
        } else if (index == 2) {
          $scope.oModal2.show();
        } else if (index == 3) {
          $scope.oModal3.show();
        } else {
          $scope.oModal4.show();
        }

      };
      $scope.closeModal = function(index) {
        if (index == 1){
          $scope.oModal1.hide();
        } else if (index == 2) {
          $scope.oModal2.hide();
        } else if (index == 3) {
          $scope.oModal3.hide();
        }else if (index == 4) {
            $scope.oModal4.hide();
        } else {
          $scope.oModal1.hide();
          $scope.oModal2.hide();
          $scope.oModal3.hide();
          $scope.oModal4.hide();
        }
      };

      // Cleanup the modals when we're done with them (i.e: state change)
      // Angular will broadcast a $destroy event just before tearing down a scope
      // and removing the scope from its parent.
      $scope.$on('$destroy', function() {
        $scope.oModal1.remove();
        $scope.oModal2.remove();
        $scope.oModal3.remove();
        $scope.oModal4.remove();
      });

    })
.controller('EsqueceuSenhaCtrl', function($scope) {
  angular.element(document.getElementById("content")).removeClass("has-header");
})
.controller('CadastrarCtrl', function($scope, $ionicModal) {
    angular.element(document.getElementById("content")).addClass("has-header");
    $ionicModal.fromTemplateUrl('templates/completar_cadastro.html', {
        id: '1', // We need to use and ID to identify the modal that is firing the event!
        scope: $scope,
        backdropClickToClose: false,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.oModal1 = modal;
    });
    $scope.openModal = function(index) {
        if (index == 1) {
            $scope.oModal1.show();
        }
    };
    $scope.closeModal = function(index) {
        if (index == 1){
            $scope.oModal1.hide();
        }
    };

    // Cleanup the modals when we're done with them (i.e: state change)
    // Angular will broadcast a $destroy event just before tearing down a scope
    // and removing the scope from its parent.
    $scope.$on('$destroy', function() {
        $scope.oModal1.remove();
    });
})
.controller('CompletarCadastroCtrl', function($scope) {
    angular.element(document.getElementById("content")).addClass("has-header");
})
.controller('SelecionarInteresseCtrl', function($scope) {

})
.controller('FeedCtrl', function($scope) {

})
.controller('NovoEventoCtrl', function($scope) {

})
.controller('InteresseEventoCtrl', function($scope) {

})
.controller('ConfirmouPresencaCtrl', function($scope) {

})
.controller('TalvezInteresseCtrl', function($scope) {
    
})
.controller('EmDestaqueCtrl', function($scope) {

})
.controller('PopularesCtrl', function($scope) {

})
.controller('TalvezConhecaCtrl', function($scope) {

})
.controller('EventosAmanhaCtrl', function($scope) {

})
.controller('EventosHojeCtrl', function($scope) {

})
.controller('CurtidasCtrl', function($scope) {

})
.controller('ComentariosCtrl', function($scope) {

})
.controller('CriarEventoCtrl', function($scope) {

})
.controller('CriarEvento2Ctrl', function($scope) {

})
.controller('PaginaEventoCtrl', function($scope, $location, $anchorScroll) {
    $scope.items = [
        {
            src:'img/galeria1.png',
        },
        {
            src:'img/galeria2.png',
        },
        {
            src:'img/galeria3.png',
        },
        {
            src:'img/galeria4.png',
        },
        {
            src:'img/galeria5.png',
        }
    ]
    $scope.scrollTo = function(id) {
        $location.hash(id);
        $anchorScroll();
    }
})
.controller('PaginaEventoFeedCtrl', function($scope) {
})
.controller('PaginaEventoComentariosCtrl', function($scope) {
})
.controller('EditarEventoCtrl', function($scope) {
})
.controller('EditarEventoIngressosCtrl', function($scope) {
})
.controller('EditarEventoAtracoesCtrl', function($scope) {
})
.controller('EditarEventoEstatisticasCtrl', function($scope) {
})
.controller('PessoasInteressadasCtrl', function($scope) {
})
.controller('ConfirmaramPresencaCtrl', function($scope) {
})
.controller('EventosCtrl', function($scope) {
})
.controller('EventosProduzidosPorVoceCtrl', function($scope) {
})
.controller('EventosHoje2Ctrl', function($scope) {
})
.controller('EventosCategoriaCtrl', function($scope) {
})
.controller('ExplorarCtrl', function($scope) {
})
.controller('ExplorarTodosCtrl', function($scope) {
})
.controller('ExplorarEventosCtrl', function($scope) {
})
.controller('EditarEventoFotosCtrl', function($scope) {
    $scope.items = [
        {
            src:'img/galeria6.png',
        },
        {
            src:'img/galeria2.png',
        },
        {
            src:'img/galeria3.png',
        },
        {
            src:'img/galeria4.png',
        },
        {
            src:'img/galeria1.png',
        }

    ]
})
.controller('EditarEventoVerFotosCtrl', function($scope) {
    $scope.items = [
        {
            src:'img/galeria6.png',
        },
        {
            src:'img/galeria2.png',
        },
        {
            src:'img/galeria3.png',
        },
        {
            src:'img/galeria4.png',
        },
        {
            src:'img/galeria1.png',
        }
    ]
})    
.controller('NovasFotosCtrl', function($scope) {
    $scope.items = [
        {
            src:'img/galeria1.png',
        },
        {
            src:'img/galeria2.png',
        },
        {
            src:'img/galeria3.png',
        },
        {
            src:'img/galeria4.png',
        },
        {
            src:'img/galeria5.png',
        }
    ]
})
.controller('FotosHashtagCtrl', function($scope) {
    $scope.items = [
        {
            src:'img/galeria1.png',
        },
        {
            src:'img/galeria2.png',
        },
        {
            src:'img/galeria3.png',
        },
        {
            src:'img/galeria4.png',
        },
        {
            src:'img/galeria5.png',
        }
    ]
})
/*.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});*/