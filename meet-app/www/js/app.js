// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ion-gallery'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
      .state('LoginCadastro', {
        url: '/LoginCadastro',
          templateUrl: 'templates/login_cadastro.html',
          controller: 'LoginCadastroCtrl'
      })
      .state('EsqueceuSenha', {
        url: '/EsqueceuSenha',
        templateUrl: 'templates/esqueceu_senha.html',
        controller: 'EsqueceuSenhaCtrl'
      })
      .state('CodigoVerificacao', {
        url: '/CodigoVerificacao',
        templateUrl: 'templates/codigo_verificacao.html',
        controller: 'CodigoVerificacaoCtrl'
      })
      .state('NovaSenha', {
        url: '/NovaSenha',
        templateUrl: 'templates/nova_senha.html',
        controller: 'NovaSenhaCtrl'
      })
      .state('SenhaCriada', {
        url: '/SenhaCriada',
        templateUrl: 'templates/senha_criada.html',
        controller: 'SenhaCriadaCtrl'
      })
      .state('Cadastrar', {
        url: '/Cadastrar',
        templateUrl: 'templates/cadastrar_usuario.html',
        controller: 'CadastrarCtrl'
      })
      .state('CompletarCadastro', {
          url: '/CompletarCadastro',
          templateUrl: 'templates/completar_cadastro.html',
          controller: 'CompletarCadastroCtrl'
      })
      .state('SelecionarInteresse', {
          url: '/SelecionarInteresse',
          templateUrl: 'templates/selecionar_interesse.html',
          controller: 'SelecionarInteresseCtrl'
      })
      .state('Feed', {
          url: '/Feed',
          templateUrl: 'templates/feed.html',
          controller: 'FeedCtrl'
      })
      .state('NovoEvento', {
          url: '/NovoEvento',
          templateUrl: 'templates/novo_evento.html',
          controller: 'NovoEventoCtrl'
      })
      .state('InteresseEvento', {
          url: '/InteresseEvento',
          templateUrl: 'templates/interesse_evento.html',
          controller: 'InteresseEventoCtrl'
      })
      .state('EventosAmanha', {
          url: '/EventosAmanha',
          templateUrl: 'templates/eventos_amanha.html',
          controller: 'EventosAmanhaCtrl'
      })
      .state('EventosHoje', {
          url: '/EventosHoje',
          templateUrl: 'templates/eventos_hoje.html',
          controller: 'EventosHojeCtrl'
      })
      .state('ConfirmouPresenca', {
          url: '/ConfirmouPresenca',
          templateUrl: 'templates/confirmou_presenca.html',
          controller: 'ConfirmouPresencaCtrl'
      })
      .state('TalvezInteresse', {
          url: '/TalvezInteresse',
          templateUrl: 'templates/talvez_interesse.html',
          controller: 'TalvezInteresseCtrl'
      })
      .state('EmDestaque', {
          url: '/EmDestaque',
          templateUrl: 'templates/em_destaque.html',
          controller: 'EmDestaqueCtrl'
      })
      .state('Populares', {
          url: '/Populares',
          templateUrl: 'templates/populares.html',
          controller: 'PopularesCtrl'
      })
      .state('TalvezConheca', {
          url: '/TalvezConheca',
          templateUrl: 'templates/talvez_conheca.html',
          controller: 'TalvezConhecaCtrl'
      })
      .state('NovasFotos', {
          url: '/NovasFotos',
          templateUrl: 'templates/novas_fotos.html',
          controller: 'NovasFotosCtrl'
      })
      .state('FotosHashtag', {
          url: '/FotosHashtag',
          templateUrl: 'templates/fotos_hashtag.html',
          controller: 'FotosHashtagCtrl'
      })
      .state('Curtidas', {
          url: '/Curtidas',
          templateUrl: 'templates/curtidas.html',
          controller: 'CurtidasCtrl'
      })
      .state('Comentarios', {
          url: '/Comentarios',
          templateUrl: 'templates/comentarios.html',
          controller: 'ComentariosCtrl'
      })
       // setup an abstract state for the tabs directive
      .state('tab', {
          url: '/tab',
          abstract: true,
          templateUrl: 'templates/tabs.html'
      })

      // Each tab has its own nav history stack:
      .state('tab.menu', {
          url: '/menu',
          views: {
              'tab-dash': {
                    templateUrl: 'templates/tab_menu.html',
                    controller: 'MenuCtrl'
              }
          }
      })
      .state('CriarEvento', {
          url: '/CriarEvento',
          templateUrl: 'templates/criar_evento.html',
          controller: 'CriarEventoCtrl'
      })
      .state('CriarEvento2', {
          url: '/CriarEvento2',
          templateUrl: 'templates/criar_evento2.html',
          controller: 'CriarEvento2Ctrl'
      })
      .state('PaginaEvento', {
          url: '/PaginaEvento',
          templateUrl: 'templates/pagina_evento.html',
          controller: 'PaginaEventoCtrl'
      })
      .state('PaginaEventoFeed', {
          url: '/PaginaEventoFeed',
          templateUrl: 'templates/pagina_evento_feed.html',
          controller: 'PaginaEventoFeedCtrl'
      })
      .state('PaginaEventoComentarios', {
          url: '/PaginaEventoComentarios',
          templateUrl: 'templates/pagina_evento_comentarios.html',
          controller: 'PaginaEventoComentariosCtrl'
      })
      .state('EditarEvento', {
          url: '/EditarEvento',
          templateUrl: 'templates/editar_evento.html',
          controller: 'EditarEventoCtrl'
      })
      .state('EditarEventoIngressos', {
          url: '/EditarEventoIngressos',
          templateUrl: 'templates/editar_evento_ingressos.html',
          controller: 'EditarEventoIngressosCtrl'
      })
      .state('EditarEventoAtracoes', {
          url: '/EditarEventoAtracoes',
          templateUrl: 'templates/editar_evento_atracoes.html',
          controller: 'EditarEventoAtracoesCtrl'
      })
      .state('EditarEventoFotos', {
          url: '/EditarEventoFotos',
          templateUrl: 'templates/editar_evento_fotos.html',
          controller: 'EditarEventoFotosCtrl'
      })
      .state('EditarEventoVerFotos', {
          url: '/EditarEventoVerFotos',
          templateUrl: 'templates/editar_evento_ver_fotos.html',
          controller: 'EditarEventoVerFotosCtrl'
      })
      .state('EditarEventoEstatisticas', {
          url: '/EditarEventoEstatisticas',
          templateUrl: 'templates/editar_evento_estatisticas.html',
          controller: 'EditarEventoEstatisticasCtrl'
      })
      .state('PessoasInteressadas', {
          url: '/PessoasInteressadas',
          templateUrl: 'templates/pessoas_interessadas.html',
          controller: 'PessoasInteressadasCtrl'
      })
      .state('ConfirmaramPresenca', {
          url: '/ConfirmaramPresenca',
          templateUrl: 'templates/confirmaram_presenca.html',
          controller: 'ConfirmaramPresencaCtrl'
      })
      .state('Eventos', {
          url: '/Eventos',
          templateUrl: 'templates/eventos.html',
          controller: 'EventosCtrl'
      })
      .state('EventosProduzidosPorVoce', {
          url: '/EventosProduzidosPorVoce',
          templateUrl: 'templates/eventos_produzidos_por_voce.html',
          controller: 'EventosProduzidosPorVoceCtrl'
      })
      .state('EventosHoje2', {
          url: '/EventosHoje2',
          templateUrl: 'templates/eventos_hoje_2.html',
          controller: 'EventosHoje2Ctrl'
      })
      .state('EventosCategoria', {
          url: '/EventosCategoria',
          templateUrl: 'templates/eventos_categoria.html',
          controller: 'EventosCategoriaCtrl'
      })
      .state('Explorar', {
          url: '/Explorar',
          templateUrl: 'templates/explorar.html',
          controller: 'ExplorarCtrl'
      })
      .state('ExplorarTodos', {
          url: '/ExplorarTodos',
          templateUrl: 'templates/explorar_todos.html',
          controller: 'ExplorarTodosCtrl'
      })
      .state('ExplorarEventos', {
          url: '/ExplorarEventos',
          templateUrl: 'templates/explorar_eventos.html',
          controller: 'ExplorarEventosCtrl'
      })
    /*
        .state('tab.chats', {
            url: '/chats',
            views: {
              'tab-chats': {
                templateUrl: 'templates/tab-chats.html',
                controller: 'ChatsCtrl'
              }
            }
          })
          .state('tab.chat-detail', {
            url: '/chats/:chatId',
            views: {
              'tab-chats': {
                templateUrl: 'templates/chat-detail.html',
                controller: 'ChatDetailCtrl'
              }
            }
          })

        .state('tab.account', {
          url: '/account',
          views: {
            'tab-account': {
              templateUrl: 'templates/tab-account.html',
              controller: 'AccountCtrl'
            }
          }
        });*/

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/LoginCadastro');

});
